# SIMPLE-CHAT API

## Signup
**Type**: POST

### `http://<server_uri>/signup`

**Request**:
    {
      "email" : < email >, 
      "password " : < password >
   } 

**Response**
    {
      "token" : token
    }' 

## Signin
**Type**: POST

### `http://<server_uri>/signin`

**Request**:
    {
      "email" : < email >, 
      "password " : < password >
    }

**Response**
    {
      "token" : token
    }

## Create Chat
**Type**: POST
**Headers**:
     x-access-token : < token >

### `http://<server_uri>/chat`

**Request**
    {
      "id" : <recipient_id>
    }

**Response**
    {
      "chat" : <chat_id>
    }

## Get Chats List
**Type**: GET
**Headers**:
     'x-access-token' : < token >

### `http://<server_uri>/chat`

**Response**
    {< chats list with messages >} 

## Send Messages
**Type**: POST
**Headers**:
     x-access-token : < token >

### `http://<server_uri>/message`

**Request**
    {
      "chat" : < chat_id >, 
      "content" : < message >
    }

## Get Message
**Type**: GET
**Headers**:
     x-access-token : < token >

### `http://<server_uri>/message/<message_id>`

**Response**
    {
      < message object >
    }


## Update Message
**Type**: PUT
**Headers**:
     x-access-token : < token >

### `http://<server_uri>/message/<message_id>`

**Request**
    {
      " content" : < message > 
    }
