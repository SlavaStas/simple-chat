'use strict';

require('dotenv').config();

const express    = require('express');
const app        = express();
const port       = 3000;
const mongoose   = require('mongoose');
const bodyParser = require('body-parser');
const routes     = require('./server/routes');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();

  app.options('*', (req, res) => {
    res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
    res.send();
  });
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', routes);
app.use((err, req, res, next) => {
  console.log(err);
  res.status(500).send('Something broke!')
});

let connectionString = 'mongodb://' + process.env.MONGODB_HOST + ':' + process.env.MONGODB_PORT + '/' + process.env.MONGODB_DATABASE;
let mongoOptions     = {
  useNewUrlParser:    true,
  useUnifiedTopology: true
};
mongoose.set('useCreateIndex', true);
mongoose.connect(connectionString, mongoOptions);


app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log(`server is listening on ${ port }`)
});

module.exports = app;