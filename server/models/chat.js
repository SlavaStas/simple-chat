'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const chatSchema = new Schema({
  user:      {
    type:     Schema.Types.ObjectId,
    ref:      'User',
    required: true
  },
  messages:  [
    { type: Schema.Types.ObjectId, ref: 'Message' }
  ],
  name:      String,
  createdAt: Date,
  updatedAt: Date
});

chatSchema.pre('save', function (next) {
  let currentDate = new Date();

  this.updatedAt = currentDate;

  if (!this.createdAt) this.createdAt = currentDate;
  next();
});

const Chat = mongoose.model('Chat', chatSchema);

module.exports = Chat;
