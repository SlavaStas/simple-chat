'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;
const bcrypt   = require('bcrypt');

const SALT_ROUNDS = 15;

const userSchema = new Schema({
  email:     { type: String, unique: true },
  password:  { type: String, select: true },
  chats:     Array,
  createdAt: Date,
  updatedAt: Date
});

userSchema.pre('save', function (next) {
  let currentDate = new Date();

  this.updatedAt = currentDate;

  if (!this.createdAt) this.createdAt = currentDate;
  if (!this.isModified('password')) return next();

  bcrypt.hash(this.password, SALT_ROUNDS).then(hash => {
    this.password = hash;
    next();
  }).catch(err => {
    console.log(err);
  });
});

const User = mongoose.model('User', userSchema);

module.exports = User;
