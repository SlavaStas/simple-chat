'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const messageSchema = new Schema({
  user:      { type: Schema.Types.ObjectId, ref: 'User', required: true },
  chat:      { type: Schema.Types.ObjectId, ref: 'Chat', required: true },
  content:   String,
  createdAt: Date,
  updatedAt: Date
});

messageSchema.pre('save', function (next) {
  let currentDate = new Date();

  this.updatedAt = currentDate;

  if (!this.createdAt) this.createdAt = currentDate;
  next();
});

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;
