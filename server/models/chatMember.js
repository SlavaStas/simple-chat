'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const chatMemberSchema = new Schema({
  user:      {
    type:     Schema.Types.ObjectId,
    ref:      'User',
    required: true
  },
  chat:      {
    type:     Schema.Types.ObjectId,
    ref:      'Chat',
    required: true
  },
  createdAt: Date,
  updatedAt: Date
});

chatMemberSchema.pre('save', function (next) {
  let currentDate = new Date();

  this.updatedAt = currentDate;

  if (!this.createdAt) this.createdAt = currentDate;
  next();
});

const ChatMember = mongoose.model('ChatMember', chatMemberSchema);

module.exports = ChatMember;
