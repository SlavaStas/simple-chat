'use strict';

const express = require('express');
const router  = express.Router();

const { UsersController } = require('./controllers/users-controller');
const usersController     = new UsersController();

const { ChatsController } = require('./controllers/chats-controller');
const chatsController     = new ChatsController();

const { AuthorizationController } = require('./controllers/authorization-controller');
const authorizationController     = new AuthorizationController();

const { MessagesController } = require('./controllers/messages-controller');
const messagesController     = new MessagesController();

/**
 * authorization
 */
router.post('/signup', (req, res, next) => usersController.signup(req, res, next));
router.post('/signin', (req, res, next) => usersController.signin(req, res, next));

/**
 * chat
 */
router.get('/chat',
  (req, res, next) => authorizationController.checkAuthorization(req, res, next),
  (req, res, next) => chatsController.getChats(req, res, next));
router.post('/chat',
  (req, res, next) => authorizationController.checkAuthorization(req, res, next),
  (req, res, next) => chatsController.createChat(req, res, next));

/**
 * messages
 */
router.get('/message/:mid',
  (req, res, next) => authorizationController.checkAuthorization(req, res, next),
  (req, res, next) => messagesController.getMessage(req, res, next));
router.post('/message',
  (req, res, next) => authorizationController.checkAuthorization(req, res, next),
  (req, res, next) => messagesController.createMessage(req, res, next));
router.put('/message/:mid',
  (req, res, next) => authorizationController.checkAuthorization(req, res, next),
  (req, res, next) => messagesController.updateMessage(req, res, next));


module.exports = router;