'use strict';

const { Controller }              = require('./controller');
const { AuthorizationController } = require('./authorization-controller');
const User                        = require('../models/user');
const authorizationController     = new AuthorizationController();

class UsersController extends Controller {
  constructor() {
    super('Controller');
  }

  signup(req, res, next) {
    let userData = {
      email:    req.body.email,
      password: req.body.password.toString(),
      token:    null
    };

    return User.findOne({ email: userData.email }).exec().then(user => {
      if (!user) {
        return User(userData).save().then(newUser => {
          let authToken = authorizationController.createToken(newUser._id);

          return super.success(res, {
            token: authToken
          });
        }).catch(err => {
          console.log(err);
          return super.fail(res, err);
        });
      }

      return super.fail(res, 'User Already Exists!');
    });
  }

  signin(req, res, next) {
    let email = req.body.email,
        pass  = req.body.password.toString();

    User.findOne({ email: email }).exec().then(user => {
      if (user) {
        return authorizationController.checkPassword(pass, user).then(authorized => {
          console.log('authorized: ' + authorized);
          if (authorized) {
            return super.success(res, {
              token: authorizationController.createToken(user._id)
            });
          } else {
            return super.unauthorized(res, 'Invalid password!');
          }
        }).catch(err => {
          console.log(err);
          return super.fail(res, err);
        });
      } else {
        return super.unauthorized(res, 'User is not found!')
      }
    })
  }
}

exports.UsersController = UsersController;