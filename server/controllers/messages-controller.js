'use strict';

const { Controller } = require('./controller');
const Message        = require('../models/message');
const User           = require('../models/user');
const Chat           = require('../models/chat');

class MessagesController extends Controller {
  constructor() {
    super('Controller');
  }

  createMessage(req, res, next) {
    User.findById(req.user.id).exec().then(user => {
      Chat.findById(req.body.chat).exec().then(chat => {
        let message = {
          user:    user,
          chat:    chat,
          content: req.body.content
        };
        return Message(message).save().then(
          result => {
            chat.messages.push(result);
            return chat.save().then(
              () => super.success(res, 'Message successful send'),
              err => super.fail(res, err.message)
            );
          },
          err => super.fail(res, err.message));
      });
    }).catch(err => {
      console.log(err);
      return super.fail(res, err.message);
    });
  }

  getMessage(req, res, next) {
    let messageId = req.params.mid;
    return Message.findById(messageId).exec().then(message => {
      if (message) {
        return super.success(res, message);
      } else {
        return super.fail(res, 'Message not found!');
      }
    });
  }

  updateMessage(req, res, next) {
    let messageId = req.params.mid,
        content   = req.body.content;

    return Message.findById(messageId).exec().then(message => {
      if (message) {
        message.content = content;
        message.save().then(
          () => super.success(res, 'Message successful updated!'),
          err => super.fail(res, err.message));
      } else {
        return super.fail(res, 'Message not found!');
      }
    });
  }

  getMessagesByChat(chatId) {
    return Message.find({ chat: chatId }).exec();
  }

}

exports.MessagesController = MessagesController;