'use strict';

const { Controller }         = require('./controller');
const { MessagesController } = require('./messages-controller');
const Chat                   = require('../models/chat');
const User                   = require('../models/user');
const ChatMember             = require('../models/chatMember');

const messagesController = new MessagesController();

class ChatsController extends Controller {
  constructor() {
    super('Controller');
  }

  createChat(req, res, next) {
    User.findById(req.user.id).exec().then(author => {
      if (author) {
        User.findById(req.body.id).exec().then(recipient => {
          if (recipient) {
            Chat({ user: author }).save().then(chat => {
              if (chat._id) {
                ChatMember({ chat: chat, user: author }).save();
                ChatMember({ chat: chat, user: recipient }).save();

                return super.success(res, { chat: chat._id });
              }
            }).catch(err => {
              console.log(err);
              return super.fail(res, err.message);
            });
          }
        }).catch(err => {
          console.log(err);
          return super.fail(res, err.message);
        });
      }
    }).catch(err => {
      console.log(err);
      return super.fail(res, err.message);
    });
  }

  getChats(req, res, next) {
    let userId = req.user.id;

    return ChatMember.find({ user: userId }).populate({
      path:     'chat',
      populate: {
        path: 'message'
      }
    }).exec()
      .then(memberships => {
        let chatsWithMessages = memberships.map(membership => {
          return messagesController.getMessagesByChat(membership.chat._id).then(messages => {
            membership.chat.messages = messages;
            // console.log(membership.chat)
            return membership.chat;
          });
        });

        return Promise.all(chatsWithMessages).then(
          result => super.success(res, result.filter(chat => chat.messages && chat.messages.length)),
          err => super.fail(res, err.message));
      });
  }

}

exports.ChatsController = ChatsController;