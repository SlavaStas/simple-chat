'use strict';

class Controller {
  constructor() {
  }

  success(res, data) {
    return res.status(200).json(data || {});
  }

  fail(res, message) {
    console.log(message);

    return res.status(400).json({
      status:  'fail',
      message: message || 'Bad request',
    });
  }

  unauthorized(res, message) {
    console.log(message);

    res.status(401).json({
      status:  'unauthorized',
      message: message || 'Authorization required',
    });
  }

  forbidden(res, message) {
    console.log(message);

    res.status(403).json({
      status:  'forbidden',
      message: message || 'Access is denied',
    });
  }

  error(res, message) {
    console.log(message);

    res.status(500).json({
      status:  'error',
      message: message || 'Internal Server Error',
    });
  }
}

exports.Controller = Controller;