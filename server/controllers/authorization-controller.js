'use strict';

const { Controller } = require('./controller');
const jwt            = require('jsonwebtoken');
const bcrypt         = require('bcrypt');
const SECRET         = process.env.SECRET;

class AuthorizationController extends Controller {
  constructor() {
    super('Controller');
  }

  createToken(userId) {
    return jwt.sign({ id: userId }, SECRET, { expiresIn: 60 * 60 });
  }

  checkPassword(pass, user) {
    return bcrypt.compare(pass, user.password);
  }

  checkAuthorization(req, res, next) {
    let token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (!token) return super.unauthorized(res, 'No token provided!');

    jwt.verify(token, SECRET, (err, decoded) => {
      if (err) {
        console.log(err);
        return super.fail(res, err.message);
      }

      req.user = {
        id:    decoded.id,
        token: this.createToken(decoded.id)
      };

    });
    next();
  }
}

exports.AuthorizationController = AuthorizationController;